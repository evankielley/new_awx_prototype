# Data Visualization in Leaflet

## Setup

### Install and run
- open a terminal then run
 - `git clone <this repository url>`
 - `cd <this repository name>`
 - `npm i`
 - `npm run start`

### View
- open web browser and navigate to
 - `http://localhost:9966/`