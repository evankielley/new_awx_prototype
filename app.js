// Initialize leaflet.js
var L = require('leaflet');
var chroma = require('chroma-js')
require('leaflet-canvaslayer-field');

var config = require('./config');

var map = L.map("map");
var url = "http://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png";
L.tileLayer(url, {
  attribution: "OSM & Carto",
  subdomains: "abcd",
  maxZoom: 19
}).addTo(map);

var layers = L.control.layers({}, {}, {collapsed: false}).addTo(map);
window.layers = layers;

const domain = 'nl';
const particleOptions = config.particleOptions[domain];

// Image
drawImageFromGeoTIFF(config.url.geotiff[domain].windSpeed);
// drawImageFromASCIIGrid(config.url.asciigrid[domain].windSpeed);

// Particles
// drawParticlesFromASCIIGrids(config.url.asciigrid[domain].windU, config.url.asciigrid[domain].windV);
// drawParticlesFromGeoTIFFs(config.url.geotiff[domain].windU, config.url.geotiff[domain].windV);
// drawParticlesFromMultibandGeoTIFF(config.url.geotiff[domain].windUV);

// Arrows
// drawArrowsFromASCIIGrid(config.url.asciigrid[domain].windDirection);
// drawArrowsFromGeoTIFF(config.url.geotiff[domain].windDirection);

// Scaled Arrows
// drawScaledArrowsFromASCIIGrids(config.url.asciigrid[domain].windU, config.url.asciigrid[domain].windV);



// Functions

// Image

function drawImageFromScalarField(s) {
  let layer = L.canvasLayer.scalarField(s, {
    type: 'colormap',
    color: chroma.scale('Spectral').domain(s.range.reverse()),
    interpolate: true
  });
//   layer.addTo(map);
  layer.setOpacity(0.5);
  layers.addBaseLayer(layer, "image");
  layer.on("click", function(e) {
    if (e.value !== null) {
      let popup = L.popup()
      .setLatLng(e.latlng)
      .setContent(`${e.value.toFixed(1)}`)
      .openOn(map);
    }
  });
  map.fitBounds(layer.getBounds());
}

function drawImageFromGeoTIFF(url) {
  fetch(url).then(r => r.arrayBuffer()).then(function(buffer) {
  var s = L.ScalarField.fromGeoTIFF(buffer);
  drawImageFromScalarField(s);
  });
}

function drawImageFromASCIIGrid(url) {
  fetch(url).then(r => r.text()).then(function (array) {
    var s = L.ScalarField.fromASCIIGrid(array);
    drawImageFromScalarField(s);
  });
}


// Particles

function drawParticlesFromVectorField(vf) {
  let layer = L.canvasLayer.vectorFieldAnim(vf, particleOptions);
  layers.addBaseLayer(layer, "particles");
  map.fitBounds(layer.getBounds());

  layer.on('click', function(e) {
    if (e.value !== null) {
      let vector = e.value;
      let v = vector.magnitude().toFixed(2);
      let d = vector.directionTo().toFixed(0);
      let html = (`${v} m/s to ${d}&deg`);
      let popup = L.popup()
      .setLatLng(e.latlng)
      .setContent(html)
      .openOn(map);
    }
  });
}

function drawParticlesFromASCIIGrids(urlU, urlV) {
  var urls = [urlU, urlV];
  var promises = urls.map(url => fetch(url).then(r => r.text()));
  Promise.all(promises).then(function (arrays) {
    let vf = L.VectorField.fromASCIIGrids(arrays[0], arrays[1]);
    drawParticlesFromVectorField(vf); 
  });
}

function drawParticlesFromGeoTIFFs(urlU, urlV) {
  var urls = [urlU, urlV];
  var promises = urls.map(url => fetch(url).then(r => r.arrayBuffer()));
  Promise.all(promises).then(function (arrays) {
    let vf = L.VectorField.fromGeoTIFFs(arrays[0], arrays[1]);
    drawParticlesFromVectorField(vf);  
  });
}

function drawParticlesFromMultibandGeoTIFF(url) {
  fetch(url).then(r => r.arrayBuffer()).then(function(buffer) {
    let vf = L.VectorField.fromMultibandGeoTIFF(buffer);
    drawParticlesFromVectorField(vf);  
  }); 
}

// Arrows

function drawArrowsFromScalarField(s) {
  let filter_nodata = function (v) { return v >= 0; };
  let layer = L.canvasLayer.scalarField(s, {
    type: 'vector',
    color: 'white', //chroma.scale(['white', 'white']).domain([-100, 100]),
    vectorSize: 20,
    arrowDirection: 'from', // from|towards to toggle arrow, if needed
    inFilter: filter_nodata
  });

  layers.addBaseLayer(layer, "arrows");
  map.fitBounds(layer.getBounds());
  layer.on('click', function (e) {
    if (e.value !== null) {
      let v = e.value.toFixed(3);
      let html = `<span class="popupText">Direction ${v}º</span>`; // from - to
      let popup = L.popup().setLatLng(e.latlng).setContent(html).openOn(map);
    }
  });
}

function drawArrowsFromASCIIGrid(url) {
  fetch(url).then(r => r.text()).then(function(buffer) {
    let s = L.ScalarField.fromASCIIGrid(buffer);
    drawArrowsFromScalarField(s);
  });
}

function drawArrowsFromGeoTIFF(url) {
  fetch(url).then(r => r.arrayBuffer()).then(function(buffer) {
      let s = L.ScalarField.fromGeoTIFF(buffer);
      drawArrowsFromScalarField(s);
  });
}


// Scaled Arrows

function drawScaledArrowsFromVectorField(vf) {
  let layer = L.canvasLayer.vectorField(vf)
  //layer.addTo(map);
  layers.addBaseLayer(layer, "scaled arrows");
  map.fitBounds(layer.getBounds());

  layer.on('click', function(e) {
    if (e.value !== null) {
      let vector = e.value;
      let v = vector.magnitude().toFixed(2);
      let d = vector.directionTo().toFixed(0);
      let html = (`${v} m/s to ${d}&deg`);
      let popup = L.popup()
      .setLatLng(e.latlng)
      .setContent(html)
      .openOn(map);
    }
  });
}

function drawScaledArrowsFromASCIIGrids(urlU, urlV) {
  // Example of vector field (scaled arrows)
  var urls = [urlU, urlV];
  var promises = urls.map(url => fetch(url).then(r => r.text()));
  Promise.all(promises).then(function (arrays) {
    let vf = L.VectorField.fromASCIIGrids(arrays[0], arrays[1]);
    drawScaledArrowsFromVectorField(vf); 
  });
}
